package com.gmf.mdrqa;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.github.ybq.android.spinkit.style.Wave;
import com.gmf.mdrqa.adapter.MaintPlanAdapter;
import com.gmf.mdrqa.model.ObjMaintPlan;
import com.gmf.mdrqa.model.ObjUploadImage;
import com.gmf.mdrqa.service.BgJobImageUploadService;
import com.gmf.mdrqa.util.ConstantVar;
import com.gmf.mdrqa.adapter.DefectLimitAdapter;
import com.gmf.mdrqa.util.Helper;
import com.gmf.mdrqa.adapter.ImageListAdapter;
import com.gmf.mdrqa.util.PopupCauseCode;
import com.gmf.mdrqa.util.PopupMDRcode;
import com.gmf.mdrqa.adapter.PsiAdapter;
import com.gmf.mdrqa.adapter.StaTypeAdapter;
import com.gmf.mdrqa.model.ObjDefectLimit;
import com.gmf.mdrqa.model.ObjPSI;
import com.gmf.mdrqa.model.ObjStaType;
import com.gmf.mdrqa.model.ResponseApi;
import com.gmf.mdrqa.network.RetrofitClientInstance;
import com.gmf.mdrqa.service.ApiService;
import com.gmf.mdrqa.util.PopupObjectPart;
import com.gmf.mdrqa.util.PopupPlant;
import com.gmf.mdrqa.util.PopupWorkCenter;
import com.google.gson.internal.LinkedTreeMap;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MdrFormActivity extends AppCompatActivity {
    //LinearLayout mHidenForm;
    List<String> imageList = new ArrayList<>();

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    int galeri=0;
    Button mBtnSubmit, mBtnCancel, mBtnChoose, mBtnCapture;
    Button mBtnMdrCode, mBtnCauseCode, mBtnObjPart;
    ImageView mImgPlant, mImgWctr;
    //MyAdapter imgAdapter;
    Spinner mComboDefect,mComboPSI,mComboStatype,mComboMaintPlan;

    EditText mTxtDefectTitle, mTxtOprnRemake,
        mTxtStaFrom, mTxtStaTo,
        mTxtStringFrom, mTxtStringTo,
        mTxtWLFrom, mTxtWLTo,
        mTxtBLFrom, mTxtBLTo,
        mTxtClock,mTxtZone,
        mTxtCauseText, mTxtPlant, mTxtWCenter
    ;
    PopupMDRcode popMdr;
    PopupCauseCode popCause;
    PopupObjectPart popObjpart;
    PopupPlant popPlant;
    PopupWorkCenter popWorkCenter;

    SharedPreferences mPrefSession;

    private RelativeLayout mLoadingFrm;
    private ProgressBar mLoadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gmf.mdrqa.R.layout.activity_mdr_form);
        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        mPrefSession = getSharedPreferences(ConstantVar.PREF_SESSION,0);

        //setup loading
        mLoadingFrm = (RelativeLayout) findViewById(com.gmf.mdrqa.R.id.loadingfrm);
        Wave loadingstyle = new Wave();
        loadingstyle.setColor(Color.WHITE);
        mLoadingBar = (ProgressBar) findViewById(com.gmf.mdrqa.R.id.loadingbar);
        mLoadingBar.setIndeterminateDrawable(loadingstyle);

        //define Text
        mTxtDefectTitle = findViewById(com.gmf.mdrqa.R.id.txtDefectTitle);
        mTxtOprnRemake = findViewById(com.gmf.mdrqa.R.id.txtRemake);
        mTxtStaFrom = findViewById(com.gmf.mdrqa.R.id.txtStaFrom);
        mTxtStaTo = findViewById(com.gmf.mdrqa.R.id.txtStaTo);
        mTxtStringFrom = findViewById(com.gmf.mdrqa.R.id.txtStringFrom);
        mTxtStringTo = findViewById(com.gmf.mdrqa.R.id.txtStringTo);
        mTxtWLFrom = findViewById(com.gmf.mdrqa.R.id.txtWLFrom);
        mTxtWLTo = findViewById(com.gmf.mdrqa.R.id.txtWLTo);
        mTxtBLFrom = findViewById(com.gmf.mdrqa.R.id.txtBLFrom);
        mTxtBLTo = findViewById(com.gmf.mdrqa.R.id.txtBLTo);
        mTxtClock = findViewById(com.gmf.mdrqa.R.id.txtClock);
        mTxtZone = findViewById(com.gmf.mdrqa.R.id.txtZone);
        mTxtCauseText = findViewById(com.gmf.mdrqa.R.id.txtCauseText);
        mTxtPlant = findViewById(R.id.txtPlant);
        mTxtWCenter = findViewById(R.id.txtWCenter);

        mTxtPlant.setText(mPrefSession.getString(ConstantVar.PREF_WORK_CENTER,null));
        mTxtWCenter.setText(mPrefSession.getString(ConstantVar.PREF_M_WORK_CENTER,null));

        recyclerView = findViewById(com.gmf.mdrqa.R.id.listimage);
        //recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setHasFixedSize(true);

        mComboPSI = findViewById(com.gmf.mdrqa.R.id.combo1);
        mComboStatype = findViewById(com.gmf.mdrqa.R.id.combo2);
        mComboDefect = findViewById(com.gmf.mdrqa.R.id.combo3);
        mComboMaintPlan = findViewById(R.id.comboMaint);

        //construct Combobox
        addItemsOnMaintPlan();
        addItemsOnPSI();
        addItemsOnStationType();
        addItemsOnDefectLimit();

        mBtnMdrCode = findViewById(com.gmf.mdrqa.R.id.btnmdrcode);
        mBtnCauseCode = findViewById(com.gmf.mdrqa.R.id.btncausecode);
        mBtnObjPart = findViewById(com.gmf.mdrqa.R.id.btnobpart);

        mBtnChoose = (Button) findViewById(com.gmf.mdrqa.R.id.btnchoose);
        mBtnCapture = (Button) findViewById(com.gmf.mdrqa.R.id.btncapture);
        mBtnSubmit = (Button) findViewById(com.gmf.mdrqa.R.id.btnsubmit);
        mBtnCancel = (Button) findViewById(com.gmf.mdrqa.R.id.btncancel);

        mImgPlant = findViewById(R.id.imgPlant);
        mImgWctr = findViewById(R.id.imgWcenter);

        popMdr = new PopupMDRcode(MdrFormActivity.this);
        popObjpart = new PopupObjectPart(MdrFormActivity.this);
        popCause = new PopupCauseCode(MdrFormActivity.this);
        popPlant = new PopupPlant(MdrFormActivity.this);
        popWorkCenter = new PopupWorkCenter(MdrFormActivity.this);

        mBtnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recyclerView.getChildCount()>4){
                    new PrettyDialog(MdrFormActivity.this)
                            .setTitle("Info")
                            .setMessage("Image Tidak Boleh Lebih Dari 5")
                            .show();
                    return;
                }

                galeri=2;
//                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(intent);
//                if (ContextCompat.checkSelfPermission(MdrFormActivity.this, Manifest.permission.CAMERA)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(MdrFormActivity.this,
//                            new String[]{Manifest.permission.CAMERA},
//                            0);
//                }
//                try{
//                    cameraIntent();
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
                boolean resultDir= Helper.checkPermission(MdrFormActivity.this);
                boolean resultCam=Helper.checkPermissionCamera(MdrFormActivity.this);
                boolean resultWrite=Helper.checkPermissionWrite(MdrFormActivity.this);
                if(resultCam && resultDir && resultWrite)
                    cameraIntent();
            }
        });

        mBtnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recyclerView.getChildCount() > 4){
                    new PrettyDialog(MdrFormActivity.this)
                            .setTitle("Info")
                            .setMessage("Image Tidak Boleh Lebih Dari 5")
                            .show();
                    return;
                }
//                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(pickPhoto , 1);
                galeri=1;
                boolean result = Helper.checkPermission(MdrFormActivity.this);
                if(result)
                    galleryIntent();
            }
        });

        mImgPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popPlant.popupdialog();
            }
        });
        mImgWctr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWorkCenter.popupdialog();
            }
        });

        mBtnMdrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popMdr.popupdialog();
            }
        });
        mBtnObjPart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popObjpart.popupdialog();
            }
        });
        mBtnCauseCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popCause.popupdialog();
            }
        });
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //insertMDR();
                //uploadimage();
                submitForm();
            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent k = new Intent(getApplicationContext(), MdrScanActivity.class);
                startActivity(k);
                finish();
            }
        });
    }

    private void submitForm(){
        Log.d("aaaaa","mdr = "+popMdr.getM_mdrcode()+" dan "+popMdr.getM_subcode());
        Log.d("aaaaa","cause = "+popCause.getM_mdrcode()+" dan "+popCause.getM_subcode());
        Log.d("aaaaa","objpart = "+popObjpart.getM_mdrcode()+" dan "+popObjpart.getM_subcode());

        if(popMdr.getM_mdrcode() == null){
            new PrettyDialog(MdrFormActivity.this)
                    .setTitle("Failed")
                    .setMessage("Please Choose MDR Code")
                    .show();
            return;
        }
        if(TextUtils.isEmpty(mTxtDefectTitle.getText())){
            mTxtDefectTitle.setError("Please Fill Defect Title");
            mTxtDefectTitle.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(mTxtOprnRemake.getText())){
            mTxtOprnRemake.setError("Please Fill Operation Remark");
            mTxtOprnRemake.requestFocus();
            return;
        }
        if(popMdr.getM_mdrcode().equals("ZPM-STR")){
            if(TextUtils.isEmpty(mTxtStaFrom.getText())){
                mTxtStaFrom.setError("Please Fill Field Station");
                mTxtStaFrom.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtStaTo.getText())){
                mTxtStaTo.setError("Please Fill Field Station To");
                mTxtStaTo.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtStringFrom.getText())){
                mTxtStringFrom.setError("Please Fill Field Stringer");
                mTxtStringFrom.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtStringTo.getText())){
                mTxtStringTo.setError("Please Fill Field Stringer To");
                mTxtStringTo.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtWLFrom.getText())){
                mTxtWLFrom.setError("Please Fill Field WL");
                mTxtWLFrom.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtWLTo.getText())){
                mTxtWLTo.setError("Please Fill Field WL To");
                mTxtWLTo.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtBLFrom.getText())){
                mTxtBLFrom.setError("Please Fill Field BL");
                mTxtBLFrom.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtBLTo.getText())){
                mTxtBLTo.setError("Please Fill Field BL To");
                mTxtBLTo.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtZone.getText())){
                mTxtZone.setError("Please Fill Field Zone");
                mTxtZone.requestFocus();
                return;
            }
            if(TextUtils.isEmpty(mTxtClock.getText())){
                mTxtClock.setError("Please Fill Field Clock Position");
                mTxtClock.requestFocus();
                return;
            }
            if(popObjpart.getM_mdrcode() == null){
                new PrettyDialog(MdrFormActivity.this)
                        .setTitle("Failed")
                        .setMessage("Please Choose Object Part")
                        .show();
                return;
            }
            if(popCause.getM_mdrcode() == null){
                new PrettyDialog(MdrFormActivity.this)
                        .setTitle("Failed")
                        .setMessage("Please Choose Cause Code")
                        .show();
                return;
            }
            if(TextUtils.isEmpty(mTxtCauseText.getText())){
                mTxtCauseText.setError("Please Fill Field Cause Text");
                mTxtCauseText.requestFocus();
                return;
            }
        }
        PrettyDialog pDialog = new PrettyDialog(MdrFormActivity.this);
        pDialog.setIcon(com.gmf.mdrqa.R.drawable.pdlg_icon_info)
                .setIconTint(com.gmf.mdrqa.R.color.pdlg_color_gray)
                .setTitle("Are you sure to create MDR Order?")
                .addButton(
                        "OK",     // button text
                        com.gmf.mdrqa.R.color.pdlg_color_white,  // button text color
                        com.gmf.mdrqa.R.color.pdlg_color_green,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                                sendTrans();
                            }
                        }
                )
                .addButton(
                        "Cancel",
                        com.gmf.mdrqa.R.color.pdlg_color_white,
                        com.gmf.mdrqa.R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        }
                )
                .setAnimationEnabled(true)
                .show();
//        if(recyclerView.getChildCount()>0){
//            uploadImage();
//        }
    }

    private Uri photoUri;
    // add items into spinner dynamically
    void cameraIntent(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            File file = null;
            try {
                file = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            photoUri = null;
            if (file != null) {
                photoUri = Uri.fromFile(file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(intent, 2);
            }
        }
        //startActivityForResult(intent, 2);
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES +"/MDRCreationImage/");
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/MDRCreationImage/");
        //File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +"/MDRCreation/images/");
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MDRCreation/images");
        if(!storageDir.exists()) storageDir.mkdirs();

        return File.createTempFile(timeStamp, ".jpg", storageDir);
    }

    void galleryIntent(){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
//            case Helper.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if(galeri==1)
//                        galleryIntent();
//                    else if(galeri==2){
//                        cameraIntent();
//                    }
//                }
//                break;
            case Helper.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    cameraIntent();
                }
                break;
            case Helper.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(galeri==1)
                        galleryIntent();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1)
                onSelectFromGalleryResult(data);
            else if (requestCode == 2){
                Log.d("zzzkamera",photoUri.getPath());
                imageList.add(photoUri.getPath());
                galleryAddPic();
                //mAdapter = new MyAdapter(imageList);
                mAdapter = new ImageListAdapter(getApplicationContext(), imageList);
                recyclerView.setAdapter(mAdapter);

                //onCaptureImageResult(data);
            }
        }
    }
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(photoUri.getPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public String imagePath;
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();

            if (data.toString().contains("content:")) {
                imagePath = getRealPathFromURI(selectedImage);
            } else if (data.toString().contains("file:")) {
                imagePath = selectedImage.getPath();
            } else {
                imagePath = null;
            }
            Log.d("zzzgaleri",imagePath);
            imageList.add(imagePath);
            //mAdapter = new MyAdapter(imageList);
            mAdapter = new ImageListAdapter(getApplicationContext(), imageList);

            recyclerView.setAdapter(mAdapter);
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null,
                    null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    String pObjMaintPlan = "";
    public void addItemsOnMaintPlan() {
        MaintPlanAdapter mpAdapter = new MaintPlanAdapter(ConstantVar.listMaintPlan, MdrFormActivity.this);
        mComboMaintPlan.setAdapter(mpAdapter);
        //spinner_country.setSelection(adapter.getPosition(myItem));//Optional to set the selected item.
        mComboMaintPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ObjMaintPlan mp = ConstantVar.listMaintPlan.get(position);
                pObjMaintPlan = mp.getKode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addItemsOnPSI() {
        PsiAdapter psiAdapter = new PsiAdapter(ConstantVar.listPSI, MdrFormActivity.this);
        mComboPSI.setAdapter(psiAdapter);
        //spinner_country.setSelection(adapter.getPosition(myItem));//Optional to set the selected item.

        mComboPSI.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("adaperor",parent.getSelectedItem().toString());
                ObjPSI psi = ConstantVar.listPSI.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    String pObjStaType = "A1";
    public void addItemsOnStationType() {

        StaTypeAdapter staAdapter = new StaTypeAdapter(ConstantVar.listStationType, MdrFormActivity.this);
        mComboStatype.setAdapter(staAdapter);
        //spinner_country.setSelection(adapter.getPosition(myItem));//Optional to set the selected item.

        mComboStatype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("adaperor",parent.getSelectedItem().toString());

                ObjStaType def = ConstantVar.listStationType.get(position);
                pObjStaType = def.getKode();
//                String pesan = "Key: "+def.getKey()+" Name: "+ def.getValue();
//                Toast.makeText(getApplicationContext(),pesan,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    String pObjDefLimit = "";
    public void addItemsOnDefectLimit() {

        DefectLimitAdapter defAdapter = new DefectLimitAdapter(ConstantVar.listDefect, MdrFormActivity.this);
        mComboDefect.setAdapter(defAdapter);
        mComboDefect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("adaperor",parent.getSelectedItem().toString());
                ObjDefectLimit def = ConstantVar.listDefect.get(position);
                pObjDefLimit = def.getKode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void sendTrans(){
        showProgress(true);
        try {
            String token = mPrefSession.getString(ConstantVar.PREF_TOKEN_KEY,null);
            String ernam = mPrefSession.getString(ConstantVar.PREF_ACCOUNT,null);
            String orig_order = mPrefSession.getString(ConstantVar.PREF_AUFNR,null);
            String floc = mPrefSession.getString(ConstantVar.PREF_FLOC,null);
//            String mworkcenter = mPrefSession.getString(ConstantVar.PREF_M_WORK_CENTER,null);
//            String workcenter = mPrefSession.getString(ConstantVar.PREF_WORK_CENTER,null);
            String pm_ps = mPrefSession.getString(ConstantVar.PREF_PMPS,null);
            String mat = mPrefSession.getString(ConstantVar.PREF_MAT,null);
            String revnr = mPrefSession.getString(ConstantVar.PREF_REVNR,null);
            String revtxt = mPrefSession.getString(ConstantVar.PREF_REVTXT,null);

            String plant = mTxtPlant.getText().toString();
            String wcenter = mTxtWCenter.getText().toString();
            String certifauth = pObjMaintPlan;

            String mdr_desc = mTxtDefectTitle.getText().toString();
            String op_text = mTxtOprnRemake.getText().toString();
            String mdr_grup = popMdr.getM_mdrcode();
            String mdr_cod = popMdr.getM_subcode();

            System.out.println(token + " | "+
                    orig_order + " | "+
                    floc + " | "+
                    pm_ps+ " | "+
                    revnr + " | "+
                    revtxt + " | "+
                    plant + " | "+
                    wcenter + " | "+
                    certifauth
            );

            String smr_sta_type = pObjStaType;
            String smr_sta_from = mTxtStaFrom.getText().toString();
            String smr_sta_to = mTxtStaTo.getText().toString();
            String smr_str_from = mTxtStringFrom.getText().toString();
            String smr_str_to = mTxtStringTo.getText().toString();
            String smr_wl_from = mTxtWLFrom.getText().toString();
            String smr_wl_to = mTxtWLTo.getText().toString();
            String smr_bl_from = mTxtBLFrom.getText().toString();
            String smr_bl_to = mTxtBLTo.getText().toString();
            String smr_zone = mTxtZone.getText().toString();
            String smr_clk_pst = mTxtClock.getText().toString();
            String smr_defect_limit = pObjDefLimit;

            String smr_objgrp = popObjpart.getM_mdrcode();
            String smr_objcod = popObjpart.getM_subcode();
            String smr_urgrp = popCause.getM_mdrcode();
            String smr_urcod = popCause.getM_subcode();
            String smr_urtxt = mTxtCauseText.getText().toString();
            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.setMdrTrans(
                    token,
                    ernam,
                    orig_order,
                    floc,
                    wcenter,
                    plant,
                    pm_ps,
                    mat,
                    revnr,
                    revtxt,
                    certifauth,

                    mdr_desc,
                    op_text,
                    mdr_grup,
                    mdr_cod,

                    smr_sta_type,
                    smr_sta_from,
                    smr_sta_to,
                    smr_str_from,
                    smr_str_to,
                    smr_wl_from,
                    smr_wl_to,
                    smr_bl_from,
                    smr_bl_to,
                    smr_zone,
                    smr_clk_pst,
                    smr_defect_limit,
                    smr_objgrp,
                    smr_objcod,
                    smr_urgrp,
                    smr_urcod,
                    smr_urtxt
            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){

                        if(response.body().getCodestatus().equalsIgnoreCase("S")){
                            Object s = (Object) response.body().getResultdata();
                            LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();
                            map = (LinkedTreeMap<String, String>) s;
                            String psnSukses = "MDR Order : "+map.get("AUFNR_NEW")
                                    +"\nNotification : "+map.get("QMNUM");
                            String idtrans=  map.get("TRANS_ID");
                            String mdr_order=map.get("AUFNR_NEW");


                            //Action Upload image
                            if(recyclerView.getChildCount()>0){
                                uploadImage(idtrans,mdr_order);
                            }

                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            PrettyDialog pDialog = new PrettyDialog(MdrFormActivity.this);
                            pDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
                            pDialog.setTitle("Success")
                                    .setIcon(com.gmf.mdrqa.R.drawable.pdlg_icon_success,
                                        com.gmf.mdrqa.R.color.pdlg_color_green,
                                        new PrettyDialogCallback() {   // icon OnClick listener
                                            @Override
                                            public void onClick() {
                                                pDialog.dismiss();
                                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                Intent k = new Intent(getApplicationContext(), MainActivity.class);
                                                k.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(k);
                                                finish();
                                            }
                                        })
                                    .setMessage(psnSukses)
                                    .show();
                        }else{
                            String err = response.body().getMessage();
                            System.out.println(err);
                            showProgress(false);
                            new PrettyDialog(MdrFormActivity.this)
                                    .setTitle("MDR Order Creation Failed")
                                    .setMessage(err)
                                    .show();
                        }
                    }else{
                        Log.d("Retro",response.message());
                        showProgress(false);
                        new PrettyDialog(MdrFormActivity.this)
                                .setTitle("MDR Order Creation Failed")
                                .setMessage("Error Found in Backend/Interface (PI)")
                                .show();
                    }
                }
                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    showProgress(false);
                    new PrettyDialog(MdrFormActivity.this)
                            .setTitle("MDR Order Creation Failed")
                            .setMessage("Connection Timeout")
                            .show();
                }
            });

        } catch(Exception e) {
            showProgress(false);
            new PrettyDialog(MdrFormActivity.this)
                    .setTitle("Failed")
                    .setMessage("Please Check Your Connection")
                    .show();
//            e.printStackTrace();
            showProgress(false);
        }
    }

    private void uploadImage(String idtrans, String mdrorder){
        //String pathx = "";
        //ListUploadImage dataImage = null;
        ObjUploadImage xdataImg;
        ArrayList<ObjUploadImage> listimg = new ArrayList<ObjUploadImage>();
        String xtoken = mPrefSession.getString(ConstantVar.PREF_TOKEN_KEY,null);
        //String uname = mPrefSession.getString(ConstantVar.PREF_ACCOUNT,null);

        for (String imgpath : imageList){
            xdataImg = new ObjUploadImage();

            xdataImg.setIdtrans(idtrans);
            xdataImg.setToken(xtoken);
            xdataImg.setImgpath(imgpath);
            xdataImg.setOrderno(mdrorder);
            //Long timestamp = System.currentTimeMillis();
            Long timestamp = TimeUnit.MILLISECONDS.toMicros(System.nanoTime());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String ndate = sdf.format(new Date());

            String nmfile = mdrorder+"_"+ndate+System.nanoTime()+".jpg";
            xdataImg.setNama(nmfile);
            xdataImg.setUrlimage("http://dev.gmf-aeroasia.com/app_mdr/public/index.php/api/v1/mdr/get-image/"+nmfile);

            listimg.add(xdataImg);
        }

        Intent inten = new Intent(MdrFormActivity.this, BgJobImageUploadService.class);
        //inten.putStringArrayListExtra("datane", datlist);
        inten.putExtra("TagParam",listimg);
        startService(inten);

    }

    private void showProgress(final boolean show) {
        if(show){
            mLoadingFrm.setVisibility(View.VISIBLE);
        }else{
            mLoadingFrm.setVisibility(View.GONE);
        }
    }
}
