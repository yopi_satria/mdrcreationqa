package com.gmf.mdrqa;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Wave;
import com.gmf.mdrqa.model.ObjMDRList;
import com.gmf.mdrqa.model.ResponseApi;
import com.gmf.mdrqa.network.RetrofitClientInstance;
import com.gmf.mdrqa.service.ApiService;
import com.gmf.mdrqa.util.ConstantVar;
import com.google.gson.internal.LinkedTreeMap;
import com.google.zxing.integration.android.IntentIntegrator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import libs.mjn.prettydialog.PrettyDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SummaryActivity extends AppCompatActivity {

    EditText mtxtRevNumber, mtxtAirReg, mtxtdate, mtxtdateto;
    EditText mtxtOrig,mtxtDefect;
    SharedPreferences mPrefSession;
    Calendar calendarFrom = Calendar.getInstance();
    Calendar calendarTo = Calendar.getInstance();
    Button mbtnCek,mbtnScan,mbtnSearch;
    final Activity activity = this;

    LinearLayout fromdate, todate;
    Spinner mairRegistration;

    private int mSelectedIndex = 0;
    String myFormat = "dd/MM/yyyy";
    SimpleDateFormat sdf;
    private RelativeLayout mLoadingFrm;
    private ProgressBar mLoadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gmf.mdrqa.R.layout.activity_summary);

        mPrefSession = getSharedPreferences(ConstantVar.PREF_SESSION,0);

        //setup loading
        mLoadingFrm = findViewById(com.gmf.mdrqa.R.id.loadingfrm);
        Wave loadingstyle = new Wave();
        loadingstyle.setColor(Color.WHITE);
        mLoadingBar =  findViewById(com.gmf.mdrqa.R.id.loadingbar);
        mLoadingBar.setIndeterminateDrawable(loadingstyle);

        sdf = new SimpleDateFormat(myFormat, Locale.US);

        mbtnCek = (Button) findViewById(com.gmf.mdrqa.R.id.btncek);
        mbtnScan = (Button) findViewById(com.gmf.mdrqa.R.id.btnscan);
        mbtnSearch = (Button) findViewById(com.gmf.mdrqa.R.id.btnsearch);

        mtxtRevNumber = findViewById(com.gmf.mdrqa.R.id.txtrevision);
        mtxtAirReg = findViewById(com.gmf.mdrqa.R.id.txtregair);
        mairRegistration = findViewById(com.gmf.mdrqa.R.id.aireg);
        mtxtOrig = findViewById(com.gmf.mdrqa.R.id.orignumber);
        mtxtDefect = findViewById(com.gmf.mdrqa.R.id.txtdefect);

        mtxtdate =  findViewById(com.gmf.mdrqa.R.id.textdate);
        mtxtdateto =  findViewById(com.gmf.mdrqa.R.id.textdateto);

        fromdate = findViewById(com.gmf.mdrqa.R.id.datefrom);
        todate = findViewById(com.gmf.mdrqa.R.id.todate);

        mtxtAirReg.setFocusable(false);

        mbtnCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mtxtRevNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mtxtAirReg.setText("");
//                if(mtxtRevNumber.length()>0)
//                {   mtxtAirReg.setVisibility(View.GONE);
//                }else{
//                    mtxtAirReg.setVisibility(View.VISIBLE);
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mbtnCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchAircraft();
            }
        });

        mbtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    cekForm();
//                    Intent k = new Intent(getApplicationContext(), SummaryListActivity.class);
//                    startActivity(k);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mbtnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(activity);
                intentIntegrator.setDesiredBarcodeFormats(intentIntegrator.ALL_CODE_TYPES);
                intentIntegrator.setBeepEnabled(true);
                intentIntegrator.setCameraId(0);
                intentIntegrator.setPrompt("Scan Originating Number");
                intentIntegrator.setBarcodeImageEnabled(false);
                intentIntegrator.initiateScan();
            }
        });


        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendarFrom.set(Calendar.YEAR, year);
                calendarFrom.set(Calendar.MONTH, monthOfYear);
                calendarFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendarTo.set(Calendar.YEAR, year);
                calendarTo.set(Calendar.MONTH, monthOfYear);
                calendarTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel2();
            }

        };

//        fromdate.setOnClickListener(v -> setFromdate(date));
//        todate.setOnClickListener(v -> setTodate(date2));
        mtxtdate.setOnClickListener(v -> setFromdate(date));
        mtxtdateto.setOnClickListener(v -> setTodate(date2));
        initSpinner();

    }
    private void setFromdate(DatePickerDialog.OnDateSetListener date){
        new DatePickerDialog(SummaryActivity.this, date, calendarFrom
                .get(Calendar.YEAR), calendarFrom.get(Calendar.MONTH),
                calendarFrom.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setTodate(DatePickerDialog.OnDateSetListener date2){
        new DatePickerDialog(SummaryActivity.this, date2, calendarTo
                .get(Calendar.YEAR), calendarTo.get(Calendar.MONTH),
                calendarTo.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {
//        String myFormat = "dd/MM/yyyy"; //In which you need put here
//        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        mtxtdate.setText(sdf.format(calendarFrom.getTime()));
    }
    private void updateLabel2() {
//        String myFormat = "dd/MM/yyyy"; //In which you need put here
//        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        mtxtdateto.setText(sdf.format(calendarTo.getTime()));
    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        mtxtOrig = findViewById(com.gmf.mdrqa.R.id.orignumber);
        if (resultCode == RESULT_OK) {
            String ordernumb =intent.getStringExtra("SCAN_RESULT");

            String tandaseru = ordernumb.substring(0,1);
            String strOut = null;
            if(tandaseru.equals("!")){
                //ambil 9 angka dari angka ke 5
                strOut = ordernumb.substring(4,13); // scan operation
            }else{
                strOut = ordernumb; // scan no order
            }
            mtxtOrig.setText(strOut);

        }else if (resultCode == RESULT_CANCELED) {
            // Handle cancels
            Toast toast = Toast.makeText(this, "Scan was Cancelled!", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    List<String> flowers = new ArrayList<>();
    ArrayAdapter mAdapter;

    private void initSpinner(){

        flowers.add("Aircraft Registration");

        mAdapter = new ArrayAdapter<String>(this, com.gmf.mdrqa.R.layout.support_simple_spinner_dropdown_item, flowers) {
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(getResources().getColor(com.gmf.mdrqa.R.color.colorWhite));
                return tv;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                tv.setTextColor(getResources().getColor(com.gmf.mdrqa.R.color.colorBlack));
                if (position == mSelectedIndex) {
                    tv.setTextColor(getResources().getColor(com.gmf.mdrqa.R.color.colorPrimaryDark));
                }
                return tv;
            }
        };
        mairRegistration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedIndex = i;
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mAdapter.setNotifyOnChange(true);
        mairRegistration.setAdapter(mAdapter);
        mairRegistration.getBackground().setColorFilter(getResources().getColor(com.gmf.mdrqa.R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
    }

    private void dataspinner(){
        flowers = new ArrayList<>();
        flowers.add("PK-GLS");
        flowers.add("PK-TKS");
        flowers.add("PK-RTG");
        flowers.add("PK-SUB");


        mAdapter = new ArrayAdapter<String>(this, com.gmf.mdrqa.R.layout.support_simple_spinner_dropdown_item, flowers){

            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(getResources().getColor(com.gmf.mdrqa.R.color.colorWhite));
                return tv;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                tv.setTextColor(getResources().getColor(com.gmf.mdrqa.R.color.colorBlack));
                if (position == mSelectedIndex) {
                    tv.setTextColor(getResources().getColor(com.gmf.mdrqa.R.color.colorPrimaryDark));
                }
                return tv;
            }
        };
        mAdapter.notifyDataSetChanged();

//        mairRegistration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                mSelectedIndex = i;
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        mairRegistration.setAdapter(mAdapter);

    }
    private void cekForm(){
        if(TextUtils.isEmpty(mtxtRevNumber.getText()) && TextUtils.isEmpty(mtxtOrig.getText()) ){
            if(TextUtils.isEmpty(mtxtRevNumber.getText())){
                mtxtRevNumber.setError("Please Fill Revision Number");
                mtxtRevNumber.requestFocus();
                return;
            }else{
                mtxtOrig.setError("Please Fill Originating Order");
                mtxtOrig.requestFocus();
                return;
            }
        }

        searchAction();
    }

    private void searchAction(){
        showProgress(true);
        try {

            String token = mPrefSession.getString(ConstantVar.PREF_TOKEN_KEY,null);
            String revnr = mtxtRevNumber.getText().toString();
            String airreg = mtxtAirReg.getText().toString();
            String datefr = mtxtdate.getText().toString();
            String dateto = mtxtdateto.getText().toString();
            String origin = mtxtOrig.getText().toString();
            String defect = mtxtDefect.getText().toString();

            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getSummaryList(
                     token,
                     revnr,
                     airreg,
                     datefr,
                     dateto,
                     origin,
                     defect

            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){
                        if(response.body().getCodestatus().equalsIgnoreCase("S")){
    //                            Object s = (Object) response.body().getResultdata();
    //                            LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();
    //                            map = (LinkedTreeMap<String, String>) s;

                                ArrayList<ObjMDRList> datamdrlist = (ArrayList<ObjMDRList>) response.body().getResultdata();

                                ArrayList<ObjMDRList> sumlist = new ArrayList<ObjMDRList>();
                                for(int n=0;n<datamdrlist.size();n++){
                                    Object s = (Object) datamdrlist.get(n);
                                    LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();

                                    //ArrayList<Object> mapImg = new ArrayList<Object>();
                                    map = (LinkedTreeMap<String, String>) s;
//                                    mapImg = (ArrayList<Object>)map.get("imageres");
//
//                                    for (int i=0; i<mapImg.size() ;i++){
//                                        String mi = mapImg.get(i).toString();
//                                        System.out.println(mi);
//                                    }

                                    ObjMDRList ss = new ObjMDRList();
                                    ss.setMdrorder(map.get("mdrorder"));
                                    ss.setOrigorder(map.get("origorder"));
                                    ss.setMdrdate(map.get("mdrdate"));
                                    ss.setMdrdesc(map.get("mdrdesc"));
                                    ss.setLongtxt(map.get("longtxt"));

                                    ss.setRevnr(map.get("revnr"));
                                    ss.setTplnr(map.get("tplnr"));
                                    ss.setErnam(map.get("ernam"));
                                    ss.setRevtx(map.get("revtx"));
                                    ss.setMdrstatus(map.get("mdrstatus"));
                                    ss.setAttachimg(map.get("attachimg"));
                                    //System.out.println(map.get("mdrstatus"));
                                    sumlist.add(ss);
                                }

    //                            listimg.add(datamdrlist);
                            showProgress(false);
                                if (sumlist.size()>0) {
                                    Intent k = new Intent(getApplicationContext(), SummaryListActivity.class);
                                    //k.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    k.putExtra("MDRList", sumlist);
                                    startActivity(k);
                                    //    finish();
                                }else{
                                    new PrettyDialog(SummaryActivity.this)
                                            .setTitle("Failed")
                                            .setMessage("Data Not Found")
                                            .show();
                                }

                        }else{
                            String err = response.body().getMessage();
                            showProgress(false);
                            new PrettyDialog(SummaryActivity.this)
                                    .setTitle("Failed")
                                    .setMessage(err)
                                    .show();
                        }
                    }else{
                        Log.d("Retro",response.message());
                        showProgress(false);
                        new PrettyDialog(SummaryActivity.this)
                                .setTitle("Failed")
                                .setMessage(response.message())
                                .show();
                    }
                }
                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    showProgress(false);
                    new PrettyDialog(SummaryActivity.this)
                            .setTitle("Failed")
                            .setMessage("Connection Timeout")
                            .show();
                }
            });

        } catch(Exception e) {
            showProgress(false);
            new PrettyDialog(SummaryActivity.this)
                    .setTitle("Failed")
                    .setMessage("Please Check Your Connection")
                    .show();
            showProgress(false);
        }
    }

    private void searchAircraft(){
        if(TextUtils.isEmpty(mtxtRevNumber.getText())){
            mtxtRevNumber.setError("Please Fill Revision Number");
            mtxtRevNumber.requestFocus();
            return;
        }
        try {
            showProgress(true);
            mtxtAirReg.setText("");
            String token = mPrefSession.getString(ConstantVar.PREF_TOKEN_KEY,null);
            String revnr = mtxtRevNumber.getText().toString();

            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getAircraftReg(
                    token,
                    revnr
            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){
                        if(response.body().getCodestatus().equalsIgnoreCase("S")){
                            Object s = (Object) response.body().getResultdata();
                            System.out.println("aa"+s.toString()+"aa");
                            LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();
                            if(!s.toString().equals("[]")) {
                                System.out.println("masuk");
                                map = (LinkedTreeMap<String, String>) s;
                                mtxtAirReg.setText(map.get("tplnr"));
                            }else{
                                mtxtAirReg.setText("");
                                Toast.makeText(getApplicationContext(),"Aircraft Not Found",Toast.LENGTH_SHORT);
                            }

                            showProgress(false);

                        }else{
                            String err = response.body().getMessage();
                            showProgress(false);
                            new PrettyDialog(SummaryActivity.this)
                                    .setTitle("Failed")
                                    .setMessage(err)
                                    .show();
                        }
                    }else{
                        Log.d("Retro",response.message());
                        showProgress(false);
                        new PrettyDialog(SummaryActivity.this)
                                .setTitle("Failed")
                                .setMessage(response.message())
                                .show();
                    }
                }
                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    showProgress(false);
                    new PrettyDialog(SummaryActivity.this)
                            .setTitle("Failed")
                            .setMessage("Connection Timeout")
                            .show();
                }
            });

        } catch(Exception e) {
            showProgress(false);
            new PrettyDialog(SummaryActivity.this)
                    .setTitle("Failed")
                    .setMessage("Please Check Your Connection")
                    .show();
            showProgress(false);
        }
    }
    private void showProgress(final boolean show) {
        if(show){
            mLoadingFrm.setVisibility(View.VISIBLE);
        }else{
            mLoadingFrm.setVisibility(View.GONE);
        }
    }

}

