package com.gmf.mdrqa;

import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gmf.mdrqa.adapter.ImageListSummaryAdapter;
import com.gmf.mdrqa.model.ObjMDRList;
import com.gmf.mdrqa.model.ResponseApi;
import com.gmf.mdrqa.network.RetrofitClientInstance;
import com.gmf.mdrqa.service.ApiService;
import com.gmf.mdrqa.util.ConstantVar;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SummaryDetailActivity extends AppCompatActivity {
    TextView mTplnr,mAircraft, mRevision,mRevText, mMDRno, mMDRDesc, mRaised,mTglCreate, mDtlImage;
    private RecyclerView mRvImage;
    private RecyclerView.Adapter mAdapter;
    List<String> imageList = new ArrayList<>();
    SharedPreferences sharedPreferences;

    private RelativeLayout mLoadingFrm;
    private ProgressBar mLoadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gmf.mdrqa.R.layout.activity_summary_detail);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(ConstantVar.PREF_SESSION, 0);

        mTplnr = findViewById(com.gmf.mdrqa.R.id.txtplnr);
        mAircraft = findViewById(com.gmf.mdrqa.R.id.txtaircraft);
        mRevision = findViewById(com.gmf.mdrqa.R.id.txtrevnumber);
        mMDRno = findViewById(com.gmf.mdrqa.R.id.txtmdrno);
        mMDRDesc = findViewById(com.gmf.mdrqa.R.id.txtmdrdesc);
        mRaised = findViewById(com.gmf.mdrqa.R.id.txternam);
        mTglCreate = findViewById(com.gmf.mdrqa.R.id.txtcreated);
        mRevText = findViewById(com.gmf.mdrqa.R.id.txtrevisiontext);
        mDtlImage = findViewById(com.gmf.mdrqa.R.id.dtlimg);

        ArrayList<ObjMDRList> myObj = (ArrayList<ObjMDRList>) getIntent().getSerializableExtra("dataMDR");
        String revText = getIntent().getStringExtra("revText");

        mTplnr.setText(myObj.get(0).getTplnr());
        mAircraft.setText(myObj.get(0).getTplnr());
        mRevision.setText(myObj.get(0).getRevnr());
        mRevText.setText(revText);
        mMDRno.setText(myObj.get(0).getMdrorder());

        if(TextUtils.isEmpty(myObj.get(0).getLongtxt())){
            mMDRDesc.setText(myObj.get(0).getMdrdesc());
        }else{
            mMDRDesc.setText(myObj.get(0).getLongtxt());
        }

        mRaised.setText("Raised by : "+myObj.get(0).getErnam());
        mTglCreate.setText(myObj.get(0).getMdrdate());



        mRvImage = findViewById(com.gmf.mdrqa.R.id.rvimage);
        mRvImage.setLayoutManager(new GridLayoutManager(this, 2));
        mRvImage.setHasFixedSize(true);

        loadImage(myObj.get(0).getMdrorder());

    }

    private void loadImage(String mdrOrder)  {
        //showProgress(true);
        try {
            String token = sharedPreferences.getString(ConstantVar.PREF_TOKEN_KEY, null);
            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getSummaryDetail(
                    token,
                    mdrOrder
            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getCodestatus().equalsIgnoreCase("S")) {
                            Object s = (Object) response.body().getResultdata();
                            LinkedTreeMap<String, Object> map = new LinkedTreeMap<String, Object>();
                            ArrayList<Object> mapImg = new ArrayList<Object>();
                            if (!s.toString().equals("[]")) {
                                map = (LinkedTreeMap<String, Object>) s;
                                mapImg = (ArrayList<Object>) map.get("image");

                                if (mapImg != null){
                                    for (int i = 0; i < mapImg.size(); i++) {
                                        String urlpath = mapImg.get(i).toString();
                                        imageList.add(urlpath);
                                        System.out.println(urlpath);
                                    }
                                }
                                mAdapter = new ImageListSummaryAdapter(getApplicationContext(), imageList);
                                mRvImage.setAdapter(mAdapter);
                                if (imageList.size()>0){
                                    mDtlImage.setVisibility(View.VISIBLE);
                                }else{
                                    mDtlImage.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            mDtlImage.setVisibility(View.GONE);
                        }
                    } else {
                        Log.d("Retro",response.message());
                        mDtlImage.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    Log.d("Retro",t.toString());
//                    showProgress(false);
//                    new PrettyDialog(SummaryDetailActivity.this)
//                            .setTitle("Failed")
//                            .setMessage("Connection Timeout")
//                            .show();
                    mDtlImage.setVisibility(View.GONE);
                }
            });

        }catch (Exception e){
            Log.d("Retro",e.getMessage());
        }
    }
}
