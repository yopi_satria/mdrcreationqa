package com.gmf.mdrqa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.gmf.mdrqa.adapter.MdrListAdapter;
import com.gmf.mdrqa.model.ObjMDRList;

import java.util.ArrayList;

public class SummaryListActivity extends AppCompatActivity {

    ListView lvItems;
    ArrayList<ObjMDRList> orderArrayList ;
    String aircraftNo,revNo;
    TextView mAircraft, mRevision,mRevDesc,mCountData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.gmf.mdrqa.R.layout.activity_summary_list);

        mAircraft = findViewById(com.gmf.mdrqa.R.id.txtAirReg);
        mRevision = findViewById(com.gmf.mdrqa.R.id.txtRevNumber);
        mRevDesc = findViewById(com.gmf.mdrqa.R.id.txtRevText);
        mCountData = findViewById(com.gmf.mdrqa.R.id.txtCountData);
        orderArrayList = new ArrayList<ObjMDRList>();

        ArrayList<ObjMDRList> myObj = (ArrayList<ObjMDRList>) getIntent().getSerializableExtra("MDRList");
        for (ObjMDRList aa :  myObj){
            if(!aa.getRevtx().isEmpty()){
                mRevDesc.setText(aa.getRevtx());
            }
            aircraftNo =  aa.getTplnr();
            revNo = aa.getRevnr();
            orderArrayList.add(aa);
        }
        mAircraft.setText(aircraftNo);
        mRevision.setText(revNo);

        mCountData.setText("Found "+myObj.size()+" Record");

        lvItems = (ListView) findViewById(com.gmf.mdrqa.R.id.listorder);
        //dataSaving();
        MdrListAdapter adapter = new MdrListAdapter(getBaseContext(),orderArrayList);
        lvItems.setAdapter(adapter);

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {

                    //String pesan = "Key: "+psi.getKey()+" Name: "+ psi.getValue();
                    //Toast.makeText(getApplicationContext(),pesan,Toast.LENGTH_SHORT).show();
                    ObjMDRList order = orderArrayList.get(position);
                    ArrayList<ObjMDRList> sumlist = new ArrayList<ObjMDRList>();
                    sumlist.add(order);

                    Intent k = new Intent(getApplicationContext(), SummaryDetailActivity.class);
                    k.putExtra("dataMDR",sumlist);
                    k.putExtra("revText",mRevDesc.getText());
                    startActivity(k);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void dataSaving() {
        ObjMDRList order = new ObjMDRList();
        order.setMdrorder("11115678978635");
        order.setMdrdesc("Defect Title");
        order.setOrigorder("8765423456");
        orderArrayList.add(order);

        order = new ObjMDRList();
        order.setMdrorder("22221432431443");
        order.setMdrdesc("Defect Title");
        order.setOrigorder("9384983298");
        orderArrayList.add(order);

        order = new ObjMDRList();
        order.setMdrorder("33337897863565");
        order.setMdrdesc("Defect Title");
        order.setOrigorder("4728487547");
        orderArrayList.add(order);

        order = new ObjMDRList();
        order.setMdrorder("44445678978635");
        order.setMdrdesc("Defect Title");
        order.setOrigorder("8948934837");
        orderArrayList.add(order);
    }
}
