package com.gmf.mdrqa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gmf.mdrqa.R;
import com.gmf.mdrqa.model.ObjMaintPlan;

import java.util.ArrayList;

public class MaintPlanAdapter extends BaseAdapter {
    ArrayList<ObjMaintPlan> mpList;
    Context mContext;

    public MaintPlanAdapter(ArrayList<ObjMaintPlan> mpList, Context mContext) {
        this.mpList = mpList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return this.mpList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ObjMaintPlan objMaintPlan = mpList.get(position);
        convertView = LayoutInflater.from(mContext).inflate(R.layout.list_combobox,null);
        TextView kode = (TextView) convertView.findViewById(R.id.txkode);
        TextView val = (TextView) convertView.findViewById(R.id.txvalue);

        kode.setText(objMaintPlan.getKode());
        val.setText(objMaintPlan.getDesc());

        return convertView;
    }
}
