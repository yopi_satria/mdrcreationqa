package com.gmf.mdrqa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmf.mdrqa.R;
import com.gmf.mdrqa.model.ObjMDRList;

import java.util.ArrayList;

public class MdrListAdapter extends BaseAdapter{

    ArrayList<ObjMDRList> orderList;
    Context mContext;

    public MdrListAdapter(Context context, ArrayList<ObjMDRList> orderArrayList) {
        this.mContext = context;
        this.orderList = orderArrayList;
    }

    @Override
    public int getCount() {
        return this.orderList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ObjMDRList order = orderList.get(position);
        convertView = LayoutInflater.from(mContext).inflate(R.layout.list_order,null);

        TextView orderno = (TextView) convertView.findViewById(R.id.text1);
        TextView desc = (TextView) convertView.findViewById(R.id.text2);
        TextView orderorig = (TextView) convertView.findViewById(R.id.text3);
        TextView tglcreate = (TextView) convertView.findViewById(R.id.text4);
        TextView mdrstatus = (TextView) convertView.findViewById(R.id.text5);
        ImageView attachimg = (ImageView) convertView.findViewById(R.id.imggbr);

        orderno.setText(order.getMdrorder());
        desc.setText(order.getMdrdesc());
        orderorig.setText(order.getOrigorder());
        tglcreate.setText(order.getMdrdate());

        if(order.getMdrstatus().contains("OPEN"))
        {
            mdrstatus.setText("OPEN");
            mdrstatus.setBackgroundResource(R.drawable.status_open);
            mdrstatus.setTextColor(Color.WHITE);
        }else if(order.getMdrstatus().contains("CLOSE")){
            mdrstatus.setText("CLOSED");
            mdrstatus.setBackgroundResource(R.drawable.status_closed);
            mdrstatus.setTextColor(Color.WHITE);
        }else{
            mdrstatus.setText(order.getMdrstatus());
            mdrstatus.setBackgroundResource(R.drawable.status_release);
            mdrstatus.setTextColor(Color.RED);
        }

        if(order.getAttachimg().contains("0"))
        {
            attachimg.setVisibility(View.GONE);
        }else{
            attachimg.setVisibility(View.VISIBLE);
        }
        return convertView;
    }
}
