package com.gmf.mdrqa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gmf.mdrqa.R;
import com.gmf.mdrqa.model.ObjPlant;

import java.util.ArrayList;

public class PlantAdapter extends BaseAdapter {

    ArrayList<ObjPlant> plantList;
    Context mContext;

    public PlantAdapter(Context context, ArrayList<ObjPlant> plantArrayList) {
        this.mContext = context;
        this.plantList = plantArrayList;
    }

    @Override
    public int getCount() {
        return this.plantList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ObjPlant pln = plantList.get(position);
        convertView = LayoutInflater.from(mContext).inflate(R.layout.list_combo_plant,null);

        TextView kode = (TextView) convertView.findViewById(R.id.txkode);
        TextView val = (TextView) convertView.findViewById(R.id.txvalue);
        TextView nam = (TextView) convertView.findViewById(R.id.txname);

        kode.setText(pln.getWerks());
        val.setText(pln.getName());
        nam.setText(pln.getCity());

        return convertView;
    }
}
