package com.gmf.mdrqa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gmf.mdrqa.R;
import com.gmf.mdrqa.model.ObjWorkCenter;

import java.util.ArrayList;

public class WorkCenterAdapter extends BaseAdapter {
    ArrayList<ObjWorkCenter> workList;
    Context mContext;

    public WorkCenterAdapter(Context mContext, ArrayList<ObjWorkCenter> workList ) {
        this.workList = workList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return this.workList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ObjWorkCenter pln = workList.get(position);
        convertView = LayoutInflater.from(mContext).inflate(R.layout.list_combo_wcenter,null);

        TextView kode = (TextView) convertView.findViewById(R.id.txkode);
        TextView val = (TextView) convertView.findViewById(R.id.txvalue);
        TextView nam = (TextView) convertView.findViewById(R.id.txname);

        kode.setText(pln.getArbpl());
        val.setText(pln.getWerks());
        nam.setText(pln.getName());

        return convertView;
    }
}
