package com.gmf.mdrqa.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ListUploadImage implements Serializable{
    ArrayList<ObjUploadImage> listImage;

    public ArrayList<ObjUploadImage> getListImage() {
        return listImage;
    }

    public void setListImage(ArrayList<ObjUploadImage> listImage) {
        this.listImage = listImage;
    }
}
