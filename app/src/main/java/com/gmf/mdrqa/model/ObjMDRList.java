package com.gmf.mdrqa.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ObjMDRList implements Serializable {
    String mdrorder;
    String mdrdesc;
    String longtxt;
    String origorder;
    String mdrdate;
    String tplnr;
    String revnr;
    String revtx;
    String ernam;
    String mdrstatus;
    String sapstatus;
    String attachimg;

    public String getAttachimg() {
        return attachimg;
    }

    public void setAttachimg(String attachimg) {
        this.attachimg = attachimg;
    }


    public String getMdrorder() {
        return mdrorder;
    }

    public void setMdrorder(String mdrorder) {
        this.mdrorder = mdrorder;
    }

    public String getMdrdesc() {
        return mdrdesc;
    }

    public void setMdrdesc(String mdrdesc) {
        this.mdrdesc = mdrdesc;
    }

    public String getLongtxt() {
        return longtxt;
    }

    public void setLongtxt(String longtxt) {
        this.longtxt = longtxt;
    }

    public String getOrigorder() {
        return origorder;
    }

    public void setOrigorder(String origorder) {
        this.origorder = origorder;
    }

    public String getMdrdate() {
        return mdrdate;
    }

    public void setMdrdate(String mdrdate) {
        this.mdrdate = mdrdate;
    }

    public String getTplnr() {
        return tplnr;
    }

    public void setTplnr(String tplnr) {
        this.tplnr = tplnr;
    }

    public String getRevnr() {
        return revnr;
    }

    public void setRevnr(String revnr) {
        this.revnr = revnr;
    }

    public String getRevtx() {
        return revtx;
    }

    public void setRevtx(String revtx) {
        this.revtx = revtx;
    }

    public String getErnam() {
        return ernam;
    }

    public void setErnam(String ernam) {
        this.ernam = ernam;
    }

    public String getMdrstatus() {
        return mdrstatus;
    }

    public void setMdrstatus(String mdrstatus) {
        this.mdrstatus = mdrstatus;
    }

    public String getSapstatus() {
        return sapstatus;
    }

    public void setSapstatus(String sapstatus) {
        this.sapstatus = sapstatus;
    }
}
