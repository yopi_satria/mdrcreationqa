package com.gmf.mdrqa.model;

public class ObjMaintPlan {
    String kode, desc;

    public ObjMaintPlan(String kode, String desc) {
        this.kode = kode;
        this.desc = desc;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ObjMaintPlan){
            ObjMaintPlan c = (ObjMaintPlan)obj;
            if(c.getKode().equals(kode) && c.getDesc().equals(desc) ) return true;
        }
        return false;
    }
}
