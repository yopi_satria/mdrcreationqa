package com.gmf.mdrqa.model;

public class ObjPlant {
    String werks, name, city;


    public ObjPlant(String werks, String name, String city) {
        this.werks = werks;
        this.name = name;
        this.city = city;
    }
    public ObjPlant(){  }

    public String getWerks() {
        return werks;
    }

    public void setWerks(String werks) {
        this.werks = werks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
