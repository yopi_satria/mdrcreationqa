package com.gmf.mdrqa.model;

public class ObjWorkCenter {
    String arbpl, werks, name;

    public ObjWorkCenter(String arbpl, String werks, String name) {
        this.arbpl = arbpl;
        this.werks = werks;
        this.name = name;
    }
    public ObjWorkCenter(){ };

    public String getArbpl() {
        return arbpl;
    }

    public void setArbpl(String arbpl) {
        this.arbpl = arbpl;
    }

    public String getWerks() {
        return werks;
    }

    public void setWerks(String werks) {
        this.werks = werks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
