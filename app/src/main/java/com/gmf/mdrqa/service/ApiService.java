package com.gmf.mdrqa.service;

import com.gmf.mdrqa.model.ResponseApi;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface ApiService {
    //Test
    @GET("login-test2")
    Call<ResponseApi> getLoginTest();

    //Login Application
//    @FormUrlEncoded
//    @POST("login-user")
//    Call<ResponseApi> getLoginUser(@Field("username") String username,
//                                   @Field("password") String password);

    @FormUrlEncoded
    @POST("v1/auth/login")
    Call<ResponseApi> getLoginUser(@Field("username") String username,
                                   @Field("password") String password,
                                   @Field("manufacture") String manufacture,
                                   @Field("model") String model,
                                   @Field("version") String version,
                                   @Field("sdk") String sdk
                                   );


    @FormUrlEncoded
    @POST("v1/mdr/get-order")
    Call<ResponseApi> getCekOrder( @Field("token") String token,
                                   @Field("order") String order
                                 )  throws Exception;


//    @Field("smr_psessi") String smr_psessi,
    @FormUrlEncoded
    @POST("v2/mdr/create-mdr")
    Call<ResponseApi> setMdrTrans(
            @Field("token") String token,
            @Field("ernam") String ernam,
            @Field("orig_order") String orig_order,
            @Field("floc") String floc,
            @Field("mworkcenter") String mworkcenter,
            @Field("workcenter") String workcenter,
            @Field("pm_ps") String pm_ps,
            @Field("mat") String mat,
            @Field("revnr") String revnr,
            @Field("revtxt") String revtxt,

            @Field("certifauth") String maintplan,

            @Field("mdr_desc") String mdr_desc,
            @Field("op_text") String op_text,
            @Field("mdr_grup") String mdr_grup,
            @Field("mdr_cod") String mdr_cod,

            @Field("smr_sta_type") String smr_sta_type,
            @Field("smr_sta_from") String smr_sta_from,
            @Field("smr_sta_to") String smr_sta_to,
            @Field("smr_str_from") String smr_str_from,
            @Field("smr_str_to") String smr_str_to,
            @Field("smr_wl_from") String smr_wl_from,
            @Field("smr_wl_to") String smr_wl_to,
            @Field("smr_bl_from") String smr_bl_from,
            @Field("smr_bl_to") String smr_bl_to,
            @Field("smr_zone") String smr_zone,
            @Field("smr_clk_pst") String smr_clk_pst,
            @Field("smr_defect_limit") String smr_defect_limit,
            @Field("smr_objgrp") String smr_objgrp,
            @Field("smr_objcod") String smr_objcod,
            @Field("smr_urgrp") String smr_urgrp,
            @Field("smr_urcod") String smr_urcod,
            @Field("smr_urtxt") String smr_urtxt
    );


    @POST("v1/mdr/upload-image")
    Call<ResponseApi> setUploadImage( @Header("Content-Type") String contentType,
                                      @Body MultipartBody body
                                    );

    @FormUrlEncoded
    @GET("v1/mdr/get-image/{name}")
    Call<Object> getImage(@Path("name") String name);

    @FormUrlEncoded
    @POST("v1/master/get-plant")
    Call<ResponseApi> getPlant(
            @Field("token") String token,
            @Field("search") String search
    );

    @FormUrlEncoded
    @POST("v1/master/get-workcenter")
    Call<ResponseApi> getWorkCenter(
            @Field("token") String token,
            @Field("plant") String plant,
            @Field("search") String search
    );

    //Summary MDR
    @FormUrlEncoded
    @POST("v1/master/get-aircraft")
    Call<ResponseApi> getAircraftReg(
            @Field("token") String token,
            @Field("revnr") String revnr
    );
    @FormUrlEncoded
    @POST("v1/master/summary-list")
    Call<ResponseApi> getSummaryList(
            @Field("token") String token,
            @Field("revnr") String revnr,
            @Field("airreg") String airreg,
            @Field("datefr") String datefr,
            @Field("dateto") String dateto,
            @Field("origin") String origin,
            @Field("defect") String defect
    );

    @FormUrlEncoded
    @POST("v1/master/summary-detail")
    Call<ResponseApi> getSummaryDetail(
            @Field("token") String token,
            @Field("mdrno") String mdrno
    );

}
