package com.gmf.mdrqa.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.gmf.mdrqa.R;
import com.gmf.mdrqa.adapter.WorkCenterAdapter;
import com.gmf.mdrqa.model.ObjWorkCenter;
import com.gmf.mdrqa.model.ResponseApi;
import com.gmf.mdrqa.network.RetrofitClientInstance;
import com.gmf.mdrqa.service.ApiService;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

import libs.mjn.prettydialog.PrettyDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopupWorkCenter {
    Context mContext;
    Button mSearchPlant;
    EditText mTxtSearch,mTxtPlant,mTxtWork;
    ObjWorkCenter objwcenter;
    ArrayList<ObjWorkCenter> dataWorklist =  null;
    SharedPreferences mPrefSession;
    ListView lvItems;
    Dialog dialog;

    public PopupWorkCenter(Context mContext) {
        this.mContext = mContext;
    }
    public void popupdialog() {

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_workcenter);

        mSearchPlant = dialog.findViewById(R.id.btnSearchPlant);
        mTxtSearch = dialog.findViewById(R.id.txtsearch);
        lvItems = dialog.findViewById(R.id.wcenterList);

        mTxtPlant = ((Activity)mContext).findViewById(R.id.txtPlant);
        mTxtWork = ((Activity)mContext).findViewById(R.id.txtWCenter);

        mSearchPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchData();
            }
        });

        searchData();
        dialog.show();
        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        ((Activity)mContext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = (int) (metrics.heightPixels*0.98); //set height to 90% of total
        int width = (int) (metrics.widthPixels*0.98); //set width to 90% of total

        dialog.getWindow().setLayout(width, height); //set layout
    }
    private void searchData(){
        try {
            mPrefSession = mContext.getSharedPreferences(ConstantVar.PREF_SESSION,0);
            String token = mPrefSession.getString(ConstantVar.PREF_TOKEN_KEY,null);
            String search = mTxtSearch.getText().toString();
            String plant = mTxtPlant.getText().toString();

            /*Create handle for the RetrofitInstance interface*/
            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<ResponseApi> call = service.getWorkCenter(
                    token,
                    plant,
                    search
            );
            call.enqueue(new Callback<ResponseApi>() {
                @Override
                public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                    if(response.isSuccessful()){
                        if(response.body().getCodestatus().equalsIgnoreCase("S")){
                            Object s = (Object) response.body().getResultdata();
                            System.out.println("aa"+s.toString()+"aa");

                            if(!s.toString().equals("[]")) {

                                ArrayList<ObjWorkCenter> tmplist = (ArrayList<ObjWorkCenter>) response.body().getResultdata();
                                dataWorklist = new ArrayList<ObjWorkCenter>();
                                //System.out.println("yopi :"+dataPlantlist.size());

                                for(int n=0; n<tmplist.size(); n++){
                                    Object dt = (Object) tmplist.get(n);
                                    LinkedTreeMap<String,String> map = new LinkedTreeMap<String,String>();

                                    map = (LinkedTreeMap<String, String>) dt;

                                    ObjWorkCenter ss = new ObjWorkCenter();
                                    ss.setArbpl(map.get("arbpl"));
                                    ss.setWerks(map.get("werks"));
                                    ss.setName(map.get("name"));
                                    dataWorklist.add(ss);
                                }
                                WorkCenterAdapter adapter = new WorkCenterAdapter(mContext,dataWorklist);
                                lvItems.setAdapter(adapter);
                                lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        ObjWorkCenter op = dataWorklist.get(position);
                                        mTxtWork.setText(op.getArbpl());
                                        dialog.dismiss();
                                    }
                                });
                            }else{
                                new PrettyDialog(mContext)
                                        .setTitle("Failed")
                                        .setMessage("Data Tidak Ditemukan")
                                        .show();
                            }
                        }else{
                            String err = response.body().getMessage();
                            new PrettyDialog(mContext)
                                    .setTitle("Failed")
                                    .setMessage(err)
                                    .show();
                        }
                    }else{
                        Log.d("Retro",response.message());
                        new PrettyDialog(mContext)
                                .setTitle("Failed")
                                .setMessage(response.message())
                                .show();
                    }
                }
                @Override
                public void onFailure(Call<ResponseApi> call, Throwable t) {
                    new PrettyDialog(mContext)
                            .setTitle("Failed")
                            .setMessage("Connection Timeout")
                            .show();
                }
            });

        } catch(Exception e) {
            new PrettyDialog(mContext)
                    .setTitle("Failed")
                    .setMessage("Please Check Your Connection")
                    .show();
        }
    }
}
